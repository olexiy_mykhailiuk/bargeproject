﻿using BargeGroupingLogic.BargeGrouping;
using BargeGroupingLogic.BargeManager;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BargeGroupingLogic.Manager
{
    /// <summary>
    /// Class provides logic of grouping barges with same river name and mile marker value within mile range
    /// </summary>
    public class BargeManager
    {
        DefaultBargeEqualityComparer groupStrategy;

        public BargeManager(double mileRange = 0.5)
        {
            this.mileRange = mileRange;
            CreateNewBargeComparer();
        }

        private double mileRange;
        /// <summary>
        /// Set or get mile range
        /// </summary>
        public double MileRange
        {
            get
            {
                return mileRange;
            }
            set
            {
                mileRange = value;
                CreateNewBargeComparer();
            }
        }

        /// <summary>
        /// Method provides logic to group barges asynchronously
        /// </summary>
        public async Task<IEnumerable<BargeGroup>> GroupBargesAsync(IEnumerable<Barge> barges)
        {
            IEnumerable<BargeGroup> retVal = Enumerable.Empty<BargeGroup>();

            await Task.Run(() =>
            {
                var groups = barges.Where(brg => brg != null).AsParallel().OrderBy(it=> it.MileMarker).AsSequential().GroupBy(brg => brg, groupStrategy).AsParallel().Select(gr => new BargeGroup(gr));
                retVal = groups.ToArray() ?? Enumerable.Empty<BargeGroup>();
            });

            return retVal;
        }

        private void CreateNewBargeComparer()
        {
            groupStrategy = new DefaultBargeEqualityComparer(mileRange);
        }

    }
}
