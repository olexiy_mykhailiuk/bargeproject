﻿using BargeGroupingLogic.BargeGrouping;
using System.Collections.Generic;

namespace BargeGroupingLogic.BargeManager
{
    /// <summary>
    /// Barges with same river name and mile marker value within mile range should be grouped. 
    /// </summary>
    class DefaultBargeEqualityComparer : IEqualityComparer<Barge>
    {
        readonly double mileRange;

        public DefaultBargeEqualityComparer(double mileRange)
        {
            this.mileRange = mileRange;
        }

        public bool Equals(Barge x, Barge y)
        {
            //since BargeManager sorts barges by mile marker additional conditions checking is not needed
            return y.MileMarker <= x.MileMarker + mileRange;
        }

        public int GetHashCode(Barge obj)
        {
            return obj.River.GetHashCode();
        }
    }
}
