﻿using BargeGroupingLogic.Common;
using System.Diagnostics.Contracts;

namespace BargeGroupingLogic.BargeGrouping
{

    public class Barge : Entity
    {

        public Barge()
        {

        }

        public Barge(long id, string code, string river, double mileMarker):
            this()
        {
            //Requires
            Contract.Requires(!string.IsNullOrEmpty(code));
            Contract.Requires(mileMarker > 0);

            this.Id = id;
            this.Code = code;
            this.River = river;
            this.MileMarker = mileMarker;
        }

        /// <summary>
        /// Gets the barge Code.
        /// </summary>
        /// <value>The barge Code.</value>
        public readonly string Code;



        /// <summary>
        /// Gets or sets the name of the river the barge is currently located on.
        /// </summary>
        /// <value>The river.</value>
        public string River { get; set; }



        /// <summary>
        /// Gets or sets the barge's mile marker (i.e. location of the barge along the river).
        /// Mile markers start from the river's delta and increase towards the beginning of the river.
        /// </summary>
        /// <value>The mile marker.</value>
        public double MileMarker { get; set; }
    }
}
