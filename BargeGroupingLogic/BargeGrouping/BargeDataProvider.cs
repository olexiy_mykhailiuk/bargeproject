using System.Collections.Generic;
using System.Diagnostics.Contracts;

namespace BargeGroupingLogic.BargeGrouping
{

    /// <summary>
    /// IBargeDataProvider provides list of barges from the Database.
    /// </summary>
    [ContractClass(typeof(IBargeDataProviderContract))]
    public interface IBargeDataProvider
    {
        /// <summary>
        /// Reads the barge list.
        /// </summary>
        /// <returns>List of barges</returns>
        IEnumerable<Barge> GetBargeList();
    }

    [ContractClassFor(typeof(IBargeDataProvider))]
    abstract class IBargeDataProviderContract : IBargeDataProvider
    {

        public IEnumerable<Barge> GetBargeList()
        {
            Contract.Ensures(Contract.Result<IEnumerable<Barge>>() != null);
            throw new System.NotImplementedException();
        }
    }


    /// <summary>
    /// BargeDataProvider class imitates reading barge list from the Database.
    /// Implements <see cref="IBargeDataProvider"/>
    /// </summary>
    public class BargeDataProvider : IBargeDataProvider
    {
        /// <inheritdoc/>
        public IEnumerable<Barge> GetBargeList()
        {
            return new List<Barge>
                       {                           
                           new Barge(1, "Barge 1", "Mississippi",19.5),
                           new Barge(2, "Barge 11", "Minnesota", 19.50),
                           new Barge(3, "Barge 2", "Mississippi", 19.5),
                           new Barge(4, "Barge 3", "Mississippi", 19.4),
                           new Barge(5, "Barge 12", "Minnesota", 19.3),
                           new Barge(6, "Barge 13", "Minnesota", 19.7),
                           new Barge(7, "Barge 4", "Mississippi", 19.6),
                           new Barge(8, "Barge 21", "Minnesota", 22.5),
                           null // This NULL element is here just to check for resiliency of your solution to bad data - you may remove it to check normal operation
                       };
        }
    }
}