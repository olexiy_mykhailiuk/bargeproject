using BargeGroupingLogic.Common;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Linq;
using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;

namespace BargeGroupingLogic.BargeGrouping
{

    /// <summary>
    /// Represents a cluster of barges pulled by the same towboat.
    /// </summary>
    public class BargeGroup : ValueObject<BargeGroup>, INotifyPropertyChanged
    {
        private readonly ConcurrentDictionary<long, Barge> barges = new ConcurrentDictionary<long, Barge>();

        public BargeGroup(IEnumerable<Barge> barges)
        {
            Contract.Requires<ArgumentNullException>(barges != null);
            InitBargeGroup(barges);
        }

        public BargeGroup(Barge barge) : this(new [] { barge })
        {
            Contract.Requires<ArgumentNullException>(barge != null);
        }

        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Gets or sets the list of barges in the group.
        /// </summary>
        /// <value>The barges.</value>
        public IEnumerable<Barge> Barges
        {
            get
            {
                return barges.Values;
            }
        }

        public void AddBarge(Barge barge)
        {
            Contract.Requires<ArgumentNullException>(barge != null);

            barges.AddOrUpdate(barge.Id, barge, (key, oldValue)=> oldValue);

            NotifyPropertyChanged();
        }

        public bool IsSingleBargeGroup
        {
            get
            {
                return barges.Count == 1;
            }
        }

        protected override bool EqualsCore(BargeGroup other)
        {
            return Enumerable.SequenceEqual(Barges, other.Barges);
        }

        protected override int GetHashCodeCore()
        {
            return GetHashCode();
        }
        
        private void NotifyPropertyChanged([CallerMemberName] String propertyName = "")
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        private void InitBargeGroup(IEnumerable<Barge> brgArray)
        {
            Parallel.ForEach(brgArray, brg =>
            {
                AddBarge(brg);
            });
        }
    }

}