﻿using System;
using System.Diagnostics.Contracts;

namespace BargeGroupingLogic.Common
{
    [ContractClass(typeof(FormatFieldTranslator<,>))]
    public interface IFormatFieldTranslator<in T,out R>
    {
        string FieldName { get; }
        R TranslateFieldTo(T entity);
    }

    [ContractClassFor(typeof(IFormatFieldTranslator<,>))]
    abstract class FormatFieldTranslator<T, R> : IFormatFieldTranslator<T, R>
    {

        string fieldName = string.Empty;

        public string FieldName
        {
            get
            {
                Contract.Ensures(Contract.Result<string>().StartsWith("[") && Contract.Result<string>().EndsWith("]"));
                return fieldName;
            }
        }

        public R TranslateFieldTo(T entity)
        {
            throw new NotImplementedException();
        }
    }

}
