﻿namespace BargeGroupingLogic.Common.Exception
{
    public class BargeProjectException : System.Exception
    {
        public BargeProjectException()
        {
        }

        public BargeProjectException(string message)
            : base(message)
        {
        }

        public BargeProjectException(string message, System.Exception inner)
            : base(message, inner)
        {
        }
    }
}
