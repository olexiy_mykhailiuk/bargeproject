﻿using BargeGroupingLogic.Common;

namespace BargeGroupingLogic.PinPoint
{
    public class PinPoint : ValueObject<PinPoint>
    {
        public virtual string Title { get; protected set; }

        public PinPoint()
        {
            
        }

        public PinPoint(string title)
        {
            Title = title;
        }

        public override string ToString()
        {
            return Title;
        }

        protected override bool EqualsCore(PinPoint other)
        {
            return Title == other.Title;
        }

        protected override int GetHashCodeCore()
        {
            return Title.GetHashCode();
        }
    }
}
