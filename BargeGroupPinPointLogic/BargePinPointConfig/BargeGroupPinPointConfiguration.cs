﻿using System.Diagnostics.Contracts;

namespace BargeGroupPinPointLogic.BargePinPointConfig
{
    public class BargeGroupPinPointConfiguration
    {
        public BargeGroupPinPointConfiguration()
        {
            //initial configuration in future should be moved to some initialize method but now it's not important
            SinglePinPointTitleFormat = "1 barge, [Id]"; 
            MultiplePinPointTitleFormat = "[Count] barges";
        }

        public string SinglePinPointTitleFormat { get; set; }
        public string MultiplePinPointTitleFormat { get; set; }

        [ContractInvariantMethod]
        private void ObjectInvariant()
        {
            Contract.Invariant(!string.IsNullOrEmpty(this.SinglePinPointTitleFormat), "Title Format shouldn't be empty string");
            Contract.Invariant(!string.IsNullOrEmpty(this.MultiplePinPointTitleFormat), "Title Format shouldn't be empty string");
        }
    }
}
