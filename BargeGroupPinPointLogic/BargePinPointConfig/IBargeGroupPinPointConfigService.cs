﻿namespace BargeGroupPinPointLogic.BargePinPointConfig
{
    public interface IBargeGroupPinPointConfigService
    {
        BargeGroupPinPointConfiguration GetConfig();
        void SaveConfig(BargeGroupPinPointConfiguration config);
    }
}
