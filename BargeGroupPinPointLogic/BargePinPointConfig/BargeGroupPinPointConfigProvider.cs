﻿namespace BargeGroupPinPointLogic.BargePinPointConfig
{
    /// <summary>
    /// This realization just emulates in simple way storing and getting of the POCO config.
    /// </summary>
    public class BargeGroupPinPointConfigProvider : IBargeGroupPinPointConfigService
    {
        private BargeGroupPinPointConfiguration config = new BargeGroupPinPointConfiguration();

        public BargeGroupPinPointConfiguration GetConfig()
        {
            return new BargeGroupPinPointConfiguration { SinglePinPointTitleFormat = config.SinglePinPointTitleFormat, MultiplePinPointTitleFormat = config.MultiplePinPointTitleFormat };
        }

        public void SaveConfig(BargeGroupPinPointConfiguration config)
        {
            this.config = config;
        }
    }
}
