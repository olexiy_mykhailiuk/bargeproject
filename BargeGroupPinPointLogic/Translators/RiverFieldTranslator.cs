﻿using BargeGroupingLogic.BargeGrouping;
using System.Linq;

namespace BargeGroupPinPointLogic.Translators
{
    public class RiverFieldTranslator : ISingleBargeFieldTranslator, IMultipleBargeFieldTranslator
    {
        public string FieldName
        {
            get
            {
                return "[River]";
            }
        }

        public string TranslateFieldTo(BargeGroup group)
        {
            var rivers = group.Barges.AsParallel().Select(barge => barge.River).Distinct();
            return string.Join(",", rivers);
        }
    }
}
