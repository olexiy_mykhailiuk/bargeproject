﻿using BargeGroupingLogic.BargeGrouping;
using System.Linq;

namespace BargeGroupPinPointLogic.Translators
{
    public class MileMarkerFieldTranslator : ISingleBargeFieldTranslator, IMultipleBargeFieldTranslator
    {
        public string FieldName
        {
            get
            {
                return "[MileMarker]";
            }
        }

        public string TranslateFieldTo(BargeGroup group)
        {
            var miles = group.Barges.AsParallel().Select(barge => barge.MileMarker).Distinct();
            return string.Join(",", miles);
        }
    }
}
