﻿using BargeGroupingLogic.Common;
using BargeGroupingLogic.BargeGrouping;

namespace BargeGroupPinPointLogic.Translators
{
    public interface IMultipleBargeFieldTranslator : IFormatFieldTranslator<BargeGroup, string>
    {
    }
}
