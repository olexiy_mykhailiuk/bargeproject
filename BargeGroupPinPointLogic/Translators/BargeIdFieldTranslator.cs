﻿using BargeGroupingLogic.BargeGrouping;
using System.Linq;

namespace BargeGroupPinPointLogic.Translators
{
    public class BargeIdFieldTranslator : ISingleBargeFieldTranslator
    {
        //Id means Code in this case for Barge
        public string FieldName
        {
            get
            {
                return "[Id]";
            }
        }

        public string TranslateFieldTo(BargeGroup group)
        {
            try
            {
                return group.Barges.First().Code;
            }
            catch
            {
                return "No Barge";
            }
        }
    }
}
