﻿using System.Linq;
using BargeGroupingLogic.BargeGrouping;

namespace BargeGroupPinPointLogic.Translators
{
    public class CountFieldTranslator : ISingleBargeFieldTranslator, IMultipleBargeFieldTranslator
    {
        public string FieldName
        {
            get
            {
                return "[Count]";
            }
        }

        public string TranslateFieldTo(BargeGroup group)
        {
            return group.Barges.Count().ToString();
        }
    }
}
