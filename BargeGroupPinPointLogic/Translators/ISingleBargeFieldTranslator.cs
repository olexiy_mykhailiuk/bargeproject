﻿using BargeGroupingLogic.Common;
using BargeGroupingLogic.BargeGrouping;

namespace BargeGroupPinPointLogic.Translators
{
    public interface ISingleBargeFieldTranslator : IFormatFieldTranslator<BargeGroup, string>
    {
    }
}
