﻿using BargeGroupingLogic.BargeGrouping;
using BargeGroupingLogic.PinPoint;
using BargeGroupPinPointLogic.BargePinPointConfig;
using BargeGroupPinPointLogic.Exceptions;
using BargeGroupPinPointLogic.Model;
using BargeGroupPinPointLogic.Translators;
using System.Collections.Generic;

namespace BargeGroupPinPointLogic.Factory.MultipleBargePinPoint
{
    public class MultipleBargeGroupPinPointCreator : BargeGroupPinPointCreator, IMultipleBargeGroupPinPointCreator
    { 
        public MultipleBargeGroupPinPointCreator(IEnumerable<IMultipleBargeFieldTranslator> bargeGroupFieldTranslators):
            base(bargeGroupFieldTranslators)
        {
            
        }

        public override PinPoint CreatePinPoint(BargeGroup group, BargeGroupPinPointConfiguration titleFormat)
        {
            if (group.IsSingleBargeGroup) throw new BargePinPointCreatorException("MultipleBargeGroupPinPointCreator can't process not multiple barge group");

            return new BargePinPoint(group, fieldFormatterDict, titleFormat.MultiplePinPointTitleFormat);
        }
    }
}
