﻿using BargeGroupingLogic.BargeGrouping;
using BargeGroupingLogic.PinPoint;
using BargeGroupPinPointLogic.BargePinPointConfig;
using BargeGroupPinPointLogic.Exceptions;
using BargeGroupPinPointLogic.Model;
using BargeGroupPinPointLogic.Translators;
using System.Collections.Generic;

namespace BargeGroupPinPointLogic.Factory.SingleBargeGroupPinPoint
{
    public class SingleBargeGroupPinPointCreator : BargeGroupPinPointCreator, ISingleBargeGroupPinPointCreator
    { 
        public SingleBargeGroupPinPointCreator(IEnumerable<ISingleBargeFieldTranslator> bargeGroupFieldTranslators):
            base(bargeGroupFieldTranslators)
        {
            
        }

        public override PinPoint CreatePinPoint(BargeGroup group, BargeGroupPinPointConfiguration titleFormat)
        {            
            if (!group.IsSingleBargeGroup) throw new BargePinPointCreatorException("Group has more or less then 1 barge");

            return new BargePinPoint(group, fieldFormatterDict, titleFormat.SinglePinPointTitleFormat);
        }
    }
}
