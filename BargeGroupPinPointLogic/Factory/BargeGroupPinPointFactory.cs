﻿using System.Collections.Generic;
using System.Linq;
using BargeGroupingLogic.BargeGrouping;
using BargeGroupPinPointLogic.BargePinPointConfig;
using BargeGroupPinPointLogic.Factory.MultipleBargePinPoint;
using BargeGroupPinPointLogic.Factory.SingleBargeGroupPinPoint;
using BargeGroupingLogic.PinPoint;

namespace BargeGroupPinPointLogic.Factory
{
    public class BargeGroupPinPointFactory : IBargeGroupPinPointFactory
    {
        private readonly IBargeGroupPinPointConfigService configProvider;
        private readonly IMultipleBargeGroupPinPointCreator multiBargeGroupPinPointCreator;
        private readonly ISingleBargeGroupPinPointCreator singleBargeGroupPinPointCreator;

        public BargeGroupPinPointFactory(IBargeGroupPinPointConfigService configProvider,
            ISingleBargeGroupPinPointCreator singleBargeGroupPinPointCreator,//It could be even IEnumerable of the elements that will be transformed to some pipeline for creation of the Single barge pinpoint
            IMultipleBargeGroupPinPointCreator multiBargeGroupPinPointCreator//It could be even IEnumerable of the elements that will be transformed to some pipeline for creation of the Multiple barge pinpoint
            )
        {
            this.configProvider = configProvider;
            this.multiBargeGroupPinPointCreator = multiBargeGroupPinPointCreator;
            this.singleBargeGroupPinPointCreator = singleBargeGroupPinPointCreator;
        }

        public IEnumerable<PinPoint> GetFrom(IEnumerable<BargeGroup> group)
        {
            var config = configProvider.GetConfig();
            return group.Select(item => GetFrom(item, config));
        }

        public PinPoint GetFrom(BargeGroup group)
        {
            var config = configProvider.GetConfig();
            return GetFrom(group, config);
        }

        private PinPoint GetFrom(BargeGroup group, BargeGroupPinPointConfiguration config)
        {
            IBargeGroupPinPointCreator creator;

            if(group.IsSingleBargeGroup)
            {
                creator = singleBargeGroupPinPointCreator;    
            } 
            else
            {
                creator = multiBargeGroupPinPointCreator;
            }

            return creator.CreatePinPoint(group, config);
            
        }


    }
}
