﻿using BargeGroupingLogic.BargeGrouping;
using System.Diagnostics.Contracts;
using System;
using BargeGroupPinPointLogic.BargePinPointConfig;
using BargeGroupingLogic.PinPoint;

namespace BargeGroupPinPointLogic.Factory
{
    [ContractClass(typeof(BargeGroupPinPointCreatorContract))]
    public interface IBargeGroupPinPointCreator
    {
        PinPoint CreatePinPoint(BargeGroup group, BargeGroupPinPointConfiguration titleFormat);
    }

    [ContractClassFor(typeof(IBargeGroupPinPointCreator))]
    abstract class BargeGroupPinPointCreatorContract : IBargeGroupPinPointCreator
    {
        public PinPoint CreatePinPoint(BargeGroup group, BargeGroupPinPointConfiguration titleFormat)
        {
            Contract.Requires<ArgumentNullException>(group != null);
            Contract.Requires<ArgumentNullException>(titleFormat != null);

            Contract.Ensures(Contract.Result<PinPoint>() != null);

            throw new System.NotImplementedException();
        }
    }

}