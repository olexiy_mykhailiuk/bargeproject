﻿using BargeGroupingLogic.BargeGrouping;
using BargeGroupingLogic.PinPoint;
using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;

namespace BargeGroupPinPointLogic.Factory
{
    // could be extended by some generic interface IPinPointFactory<T>. It will be useful in case of need to inject all pinpoint factories
    [ContractClass(typeof(BargeGroupPinPointFactoryContract))]
    public interface IBargeGroupPinPointFactory
    {
       IEnumerable<PinPoint> GetFrom(IEnumerable<BargeGroup> group);
       PinPoint GetFrom(BargeGroup group);
    }

    [ContractClassFor(typeof(IBargeGroupPinPointFactory))]
    abstract class BargeGroupPinPointFactoryContract : IBargeGroupPinPointFactory
    {
        
        public IEnumerable<PinPoint> GetFrom(IEnumerable<BargeGroup> group)
        {
            Contract.Requires<ArgumentNullException>(group != null, "Can't process null Barge group array!");
 	        Contract.Ensures(Contract.Result<IEnumerable<PinPoint>>()!=null, "Returning null PinPoint collection is denied");

            return null;
        }

        public PinPoint GetFrom(BargeGroup group)
        {
            Contract.Requires<ArgumentNullException>(group != null, "Can't process null Barge group!");
            Contract.Ensures(Contract.Result<PinPoint>() != null, "Returning null Pin Point value is denied!");
            throw new NotImplementedException();
        }
    }
}
