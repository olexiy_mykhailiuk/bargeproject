﻿using BargeGroupingLogic.Common;
using BargeGroupingLogic.BargeGrouping;
using BargeGroupingLogic.PinPoint;
using BargeGroupPinPointLogic.BargePinPointConfig;
using System.Collections.Generic;
using System.Linq;

namespace BargeGroupPinPointLogic.Factory
{
    public abstract class BargeGroupPinPointCreator : IBargeGroupPinPointCreator
    {
        protected Dictionary<string, IFormatFieldTranslator<BargeGroup, string>> fieldFormatterDict;

        public BargeGroupPinPointCreator(IEnumerable<IFormatFieldTranslator<BargeGroup, string>> bargeGroupFieldTranslators)
        {
            InitializeFieldTranslatorDict(bargeGroupFieldTranslators);
        }

        private void InitializeFieldTranslatorDict(IEnumerable<IFormatFieldTranslator<BargeGroup, string>> bargeGroupFieldTranslators)
        {
            fieldFormatterDict = bargeGroupFieldTranslators.ToDictionary(item => item.FieldName);
        }

        public abstract PinPoint CreatePinPoint(BargeGroup group, BargeGroupPinPointConfiguration titleFormat);
    }
}
