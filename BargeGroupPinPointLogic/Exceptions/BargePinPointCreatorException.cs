﻿using BargeGroupingLogic.Common.Exception;
using System;

namespace BargeGroupPinPointLogic.Exceptions
{
    public class BargePinPointCreatorException : BargeProjectException
    {
        public BargePinPointCreatorException()
        {
        }

        public BargePinPointCreatorException(string message)
            : base(message)
        {
        }

        public BargePinPointCreatorException(string message, Exception inner)
            : base(message, inner)
        {
        }
    }
}
