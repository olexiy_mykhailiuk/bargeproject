﻿using BargeGroupingLogic.Common;
using BargeGroupingLogic.BargeGrouping;
using BargeGroupingLogic.PinPoint;
using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Text;
using System.Text.RegularExpressions;

namespace BargeGroupPinPointLogic.Model
{
    public class BargePinPoint : PinPoint
    {
        private readonly BargeGroup bargeGroup;
        private readonly IDictionary<string, IFormatFieldTranslator<BargeGroup, string>> bargeGroupFieldTranslators;

        public BargePinPoint(BargeGroup bargeGroup, IDictionary<string, IFormatFieldTranslator<BargeGroup, string>> bargeGroupFieldTranslators, string titleFormat)
        {
            Contract.Requires<ArgumentNullException>(bargeGroup != null);
            Contract.Requires<ArgumentNullException>(bargeGroupFieldTranslators != null);
            Contract.Requires<ArgumentNullException>(!string.IsNullOrEmpty(titleFormat));

            this.bargeGroup = bargeGroup;
            this.bargeGroupFieldTranslators = bargeGroupFieldTranslators;
            this.titleFormat = titleFormat;

            bargeGroup.PropertyChanged += bargeGroup_PropertyChanged;
        }

        private void bargeGroup_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            title = string.Empty;
        }

        string titleFormat;
        //TODO: throw exception in case value null or empty
        public string TitleFormat
        {
            set
            {
                Contract.Requires<ArgumentNullException>(string.IsNullOrEmpty(value), "Title format string can't be null or empty");
                titleFormat = value;
                title = string.Empty;
            }
            get
            {
                return titleFormat;
            }
        }

        string title;
        public override string Title
        {
            get
            {
                if (string.IsNullOrEmpty(title))
                {
                    title = CreateTitle();
                }

                return title;
            }
        }

        private string CreateTitle()
        {

            string s = titleFormat;
            // Parse the message into an array of tokens

            var reg = @"(\[[^\]]+\])";
            Regex regex = new Regex(reg);
            string[] tokens = regex.Split(s);

            // Re-build the new message from the tokens
            var sb = new StringBuilder();
            foreach (string token in tokens)
            {
                sb.Append(bargeGroupFieldTranslators.ContainsKey(token) ? bargeGroupFieldTranslators[token].TranslateFieldTo(bargeGroup) : token);
            }

            s = sb.ToString();

            return s;
        }

        protected override bool EqualsCore(PinPoint other)
        {
            var otherBargePinPoint = other as BargePinPoint;

            return otherBargePinPoint!=null && TitleFormat == otherBargePinPoint.TitleFormat && bargeGroup==otherBargePinPoint.bargeGroup && base.EqualsCore(other);
        }

    }
}
