﻿using BargeGroupingLogic.BargeGrouping;
using NUnit.Framework;
using System.Linq;

namespace BargeGroupUnitTests.BargeGroupTest
{
    [TestFixture]
    public class BargeGroupTest
    {
        Barge barge1;
        Barge barge2;
        Barge barge3;
        

        [SetUp]
        protected void SetUp()
        {
            barge1 = new Barge(1, "Barge 1", "Mississippi",19.5);
            barge2 = new Barge(2, "Barge 11", "Minnesota", 19.50);
            barge3 = new Barge(3, "Barge 2", "Mississippi", 19.5);
        }

        

        [Test]
        public void Is_BargeGroup_Qualify_Barge_Id()
        {
            var bargeGr = new BargeGroup(barge1);
            var bargeWithDiffrentIdButSameInfoAsBarge1 = new Barge(11, "Barge 1", "Mississippi", 19.5);
            var bargeHasSameIdButDifferentInfoAsBarge1 = new Barge(1, "Barge 111", "MississippiTest", 26);

            bargeGr.AddBarge(bargeWithDiffrentIdButSameInfoAsBarge1);

            Assert.AreEqual(bargeGr.Barges.Count(), 2);

            bargeGr.AddBarge(bargeHasSameIdButDifferentInfoAsBarge1);

            Assert.AreEqual(bargeGr.Barges.Count(), 2);

        }

        [Test]
        public void Check_Barges_Count()
        {
            var bargeGr = new BargeGroup(barge1);
            bargeGr.AddBarge(barge1);

            Assert.AreEqual(bargeGr.Barges.Count(), 1);

            bargeGr.AddBarge(barge2);
            bargeGr.AddBarge(barge3);

            Assert.AreEqual(bargeGr.Barges.Count(), 3);
        }

        [Test]
        public void IsSingleBargeGroup_Property_works()
        {
            var bargeGr = new BargeGroup(barge1);
            bargeGr.AddBarge(barge1);

            Assert.IsTrue(bargeGr.IsSingleBargeGroup);

            bargeGr.AddBarge(new Barge(9988, "Fake_Id", "Dnipro", 20.7));

            Assert.IsFalse(bargeGr.IsSingleBargeGroup);
        }
    }
}
