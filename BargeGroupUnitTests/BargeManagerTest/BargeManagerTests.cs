﻿using BargeGroupingLogic.BargeGrouping;
using BargeGroupingLogic.Manager;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace BargeGroupUnitTests.BargeManagerTest
{
    [TestFixture]
    public class BargeManagerTests
    {
        Mock<IBargeDataProvider> mockBargeDataProvider;
        Barge barge1;
        Barge barge2;
        Barge barge3;
        Barge barge4;
        Barge barge5;
        Barge barge6;
        Barge barge7;
        Barge barge8;

        [SetUp]
        protected void SetUp()
        {
            mockBargeDataProvider = new Mock<IBargeDataProvider>();
            barge1 = new Barge(1, "Barge 1", "Mississippi", 19.5);
            barge2 = new Barge(2, "Barge 11", "Minnesota", 19.50);
            barge3 = new Barge(3, "Barge 2", "Mississippi", 19.5);
            barge4 = new Barge(4, "Barge 3", "Mississippi", 19.4);
            barge5 = new Barge(5, "Barge 12", "Minnesota", 19.3);
            barge6 = new Barge(6, "Barge 13", "Minnesota", 19.7);
            barge7 = new Barge(7, "Barge 4", "Mississippi", 19.6);
            barge8 = new Barge(8, "Barge 21", "Minnesota", 22.5);

            mockBargeDataProvider.Setup(pr => pr.GetBargeList()).Returns(() =>
            {
                return new List<Barge>
                       {
                           barge1,
                           barge2,
                           barge3,
                           barge4,
                           barge5,
                           barge6,
                           barge7,
                           barge8,
                           null // This NULL element is here just to check for resiliency of your solution to bad data - you may remove it to check normal operation
                       };
            });
        }

        [Test]
        public void BargeManager_Does_Not_Throw_Exception_if_barge_is_null()
        {
            BargeManager bm = new BargeManager();
            var groupsTask = bm.GroupBargesAsync(mockBargeDataProvider.Object.GetBargeList());
            groupsTask.Wait();

            Assert.IsNull(groupsTask.Exception);
        }

        [Test]
        public void BargeManager_Group_Barges_Within_Mile_Range()
        {
            BargeManager bm = new BargeManager();
            var groupsTask = bm.GroupBargesAsync(mockBargeDataProvider.Object.GetBargeList());
            //group with default time range
            groupsTask.Wait();
            Assert.AreEqual(3, groupsTask.Result.Count());
            var result = groupsTask.Result.OrderBy(bg => bg.Barges.Count());

            //check if groups contain expected number of elements and expected elements
            Assert.AreEqual(1, result.ElementAt(0).Barges.Count());
            Assert.IsTrue(result.ElementAt(0).Barges.Contains(barge8)); //"Barge 21", "Minnesota", 22.5

            Assert.AreEqual(3, result.ElementAt(1).Barges.Count());
            Assert.IsTrue(result.ElementAt(1).Barges.Contains(barge2)); //"Barge 11", "Minnesota", 19.50
            Assert.IsTrue(result.ElementAt(1).Barges.Contains(barge5)); //"Barge 12", "Minnesota", 19.3
            Assert.IsTrue(result.ElementAt(1).Barges.Contains(barge6)); //"Barge 13", "Minnesota", 19.7

            Assert.AreEqual(4, result.ElementAt(2).Barges.Count());
            Assert.IsTrue(result.ElementAt(2).Barges.Contains(barge1)); //"Barge 1", "Mississippi", 19.5
            Assert.IsTrue(result.ElementAt(2).Barges.Contains(barge3)); //"Barge 2", "Mississippi", 19.5
            Assert.IsTrue(result.ElementAt(2).Barges.Contains(barge4)); //"Barge 3", "Mississippi", 19.4
            Assert.IsTrue(result.ElementAt(2).Barges.Contains(barge7)); //"Barge 4", "Mississippi", 19.6

            //change mile range runtime
            bm.MileRange = 10;
            groupsTask = bm.GroupBargesAsync(mockBargeDataProvider.Object.GetBargeList());
            groupsTask.Wait();
            Assert.AreEqual(2, groupsTask.Result.Count());
            Assert.AreEqual(4, groupsTask.Result.ElementAt(0).Barges.Count());
            Assert.AreEqual(4, groupsTask.Result.ElementAt(1).Barges.Count());
        }
    }
}
