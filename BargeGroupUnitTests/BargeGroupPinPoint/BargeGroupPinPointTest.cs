﻿using BargeGroupingLogic.Common;
using BargeGroupingLogic.BargeGrouping;
using BargeGroupPinPointLogic.Model;
using BargeGroupPinPointLogic.Translators;
using NUnit.Framework;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace BargeGroupUnitTests.BargeGroupPinPoint
{
    [TestFixture]
    public class BargeGroupPinPointTest
    {
        ReadOnlyDictionary<string, IFormatFieldTranslator<BargeGroup, string>> riverFieldDict;
        ReadOnlyDictionary<string, IFormatFieldTranslator<BargeGroup, string>> mileMarkerFieldDict;
        ReadOnlyDictionary<string, IFormatFieldTranslator<BargeGroup, string>> countFieldDict;
        ReadOnlyDictionary<string, IFormatFieldTranslator<BargeGroup, string>> bargeIdFieldDict;
        Barge barge1;

        [SetUp]
        protected void SetUp()
        {

            Dictionary<string, IFormatFieldTranslator<BargeGroup, string>> tempDict = new Dictionary<string, IFormatFieldTranslator<BargeGroup, string>>();

            var riverFieldTranslatorInst = new RiverFieldTranslator();      
            tempDict.Add(riverFieldTranslatorInst.FieldName, riverFieldTranslatorInst);
            riverFieldDict = new ReadOnlyDictionary<string, IFormatFieldTranslator<BargeGroup, string>>(tempDict);            

            var mileMarkerFieldTranslatorInst = new MileMarkerFieldTranslator();
            tempDict = new Dictionary<string, IFormatFieldTranslator<BargeGroup, string>>();
            tempDict.Add(mileMarkerFieldTranslatorInst.FieldName, mileMarkerFieldTranslatorInst);
            mileMarkerFieldDict = new ReadOnlyDictionary<string, IFormatFieldTranslator<BargeGroup, string>>(tempDict);

            var countFieldTranslatorInst = new CountFieldTranslator();
            tempDict = new Dictionary<string, IFormatFieldTranslator<BargeGroup, string>>();
            tempDict.Add(countFieldTranslatorInst.FieldName, countFieldTranslatorInst);
            countFieldDict = new ReadOnlyDictionary<string, IFormatFieldTranslator<BargeGroup, string>>(tempDict);

            var bargeIdFieldTranslatorInst = new BargeIdFieldTranslator();
            tempDict = new Dictionary<string, IFormatFieldTranslator<BargeGroup, string>>();
            tempDict.Add(bargeIdFieldTranslatorInst.FieldName, bargeIdFieldTranslatorInst);
            bargeIdFieldDict = new ReadOnlyDictionary<string, IFormatFieldTranslator<BargeGroup, string>>(tempDict);

            barge1 = new Barge(8080, "Barge 1", "Mississippi", 19.5);
        }

        [Test]
        public void Check_River_Field_Translation()
        {
            var initialStr = "Test str [River]";
            var bargeGroup = new BargeGroup(barge1);

            var bgPinPoint = new BargePinPoint(bargeGroup, riverFieldDict, initialStr);

            Assert.AreEqual(initialStr, bgPinPoint.TitleFormat); // check if title format set correctly

            Assert.AreEqual("Test str Mississippi", bgPinPoint.Title);

        }
        
        [Test]
        public void Check_MileMarker_Field_Translation()
        {
            var initialStr = "Test str [MileMarker]";
            var bargeGroup = new BargeGroup(barge1);


            var bgPinPoint = new BargePinPoint(bargeGroup, mileMarkerFieldDict, initialStr);

            Assert.AreEqual(initialStr, bgPinPoint.TitleFormat); // check if title format set correctly

            Assert.AreEqual("Test str " + 19.5, bgPinPoint.Title);
        }

        [Test]
        public void Check_Count_Field_Translation()
        {
            var initialStr = "Test str [Count]";
            var bargeGroup = new BargeGroup(barge1);

            var bgPinPoint = new BargePinPoint(bargeGroup, countFieldDict, initialStr);

            Assert.AreEqual(initialStr, bgPinPoint.TitleFormat); // check if title format set correctly

            Assert.AreEqual("Test str 1", bgPinPoint.Title);

            bargeGroup.AddBarge(new Barge(2, "Fake_Code", "Mississippi", 19.6));

            Assert.AreEqual("Test str 2", bgPinPoint.Title);
        }

        [Test]
        public void Check_Id_Field_Translation()
        {
            var initialStr = "Test str [Id]";
            var bargeGroup = new BargeGroup(barge1);

            var bgPinPoint = new BargePinPoint(bargeGroup, bargeIdFieldDict, initialStr);

            Assert.AreEqual(initialStr, bgPinPoint.TitleFormat); // check if title format set correctly

            Assert.AreEqual("Test str Barge 1", bgPinPoint.Title);
        }

        [Test]
        public void Check_Unregistered_Word_Translation()
        {
            var initialStr = "Test str [Ids]";
            var bargeGroup = new BargeGroup(barge1);

            var bgPinPoint = new BargePinPoint(bargeGroup, bargeIdFieldDict, initialStr);

            Assert.AreEqual("Test str [Ids]", bgPinPoint.Title);
        }
    }
}
