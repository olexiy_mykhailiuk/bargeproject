﻿using BargeGroupingLogic.BargeGrouping;
using BargeGroupPinPointLogic.BargePinPointConfig;
using BargeGroupPinPointLogic.Exceptions;
using BargeGroupPinPointLogic.Factory.MultipleBargePinPoint;
using BargeGroupPinPointLogic.Factory.SingleBargeGroupPinPoint;
using BargeGroupPinPointLogic.Translators;
using NUnit.Framework;
using System.Collections.Generic;

namespace BargeGroupUnitTests.PinPointCreator
{
    [TestFixture]
    public class BargeGroupPinPointCreatorsTest
    {
        
        IEnumerable<ISingleBargeFieldTranslator> bargeGroupFieldTranslators;
        IEnumerable<IMultipleBargeFieldTranslator> MultipleBargeGroupFieldTranslators;

        [SetUp]
        protected void SetUp()
        {
            bargeGroupFieldTranslators = new ISingleBargeFieldTranslator [] { new BargeIdFieldTranslator(), new RiverFieldTranslator() };
            MultipleBargeGroupFieldTranslators = new IMultipleBargeFieldTranslator[] { new CountFieldTranslator(), new RiverFieldTranslator() };            
        }

        [Test]
        public void PinPoint_Created_By_SingleCreator_With_TitleFormat_Translation()
        {
            BargeGroup bargeGroup = new BargeGroup(new Barge(9988, "Fake_Id", "Dnipro", 19.5));

            BargeGroupPinPointConfiguration config = new BargeGroupPinPointConfiguration();
            config.SinglePinPointTitleFormat = "Single Barge Group PinPoint has Title with [Id] and [River] and [Something]";

            var singleBgPpCreator = new SingleBargeGroupPinPointCreator(bargeGroupFieldTranslators);

            var pinPoint = singleBgPpCreator.CreatePinPoint(bargeGroup, config);

            Assert.AreEqual("Single Barge Group PinPoint has Title with Fake_Id and Dnipro and [Something]", pinPoint.Title);
        }

        [Test]
        public void Throw_Exception_If_Single_Creator_Process_Group_With_More_then_1_Barge()
        {
            Assert.Throws<BargePinPointCreatorException>(() => {
                BargeGroup bargeGroup = new BargeGroup(new Barge(9988, "Fake_Id", "Dnipro", 19.5));
                bargeGroup.AddBarge(new Barge(9989, "Fake_Id", "Dnipro", 19.6));

                BargeGroupPinPointConfiguration config = new BargeGroupPinPointConfiguration();

                var singleBgPpCreator = new SingleBargeGroupPinPointCreator(bargeGroupFieldTranslators);

                var pinPoint = singleBgPpCreator.CreatePinPoint(bargeGroup, config);

            }, "Group has more then 1 barge");
        }

        [Test]
        public void PinPoint_Created_By_MultipleCreator_With_TitleFormat_Translation()
        {
            BargeGroup bargeGroup = new BargeGroup(new Barge(9988, "Fake_Id", "Dnipro", 19.5));
            bargeGroup.AddBarge(new Barge(9989, "Fake_Id2", "Dnipro", 19.6));

            BargeGroupPinPointConfiguration config = new BargeGroupPinPointConfiguration();            
            config.MultiplePinPointTitleFormat = "Multiple Barge Group PinPoint has Title with [Id] and [River] and [Count]";

            var multipleBgPpCreator = new MultipleBargeGroupPinPointCreator(MultipleBargeGroupFieldTranslators);            

            var pinPoint = multipleBgPpCreator.CreatePinPoint(bargeGroup, config);

            Assert.AreEqual("Multiple Barge Group PinPoint has Title with [Id] and Dnipro and 2", pinPoint.Title);
        }

        [Test]
        public void Throw_Exception_If_Multiple_Creator_Process_Not_Multiple_Barge_Group()
        {
            Assert.Throws<BargePinPointCreatorException>(() =>
            {
                BargeGroup bargeGroup = new BargeGroup(new Barge(9988, "Fake_Id", "Dnipro", 19.5));

                BargeGroupPinPointConfiguration config = new BargeGroupPinPointConfiguration();

                var multipleBgPpCreator = new MultipleBargeGroupPinPointCreator(MultipleBargeGroupFieldTranslators);

                var pinPoint = multipleBgPpCreator.CreatePinPoint(bargeGroup, config);

            }, "MultipleBargeGroupPinPointCreator can't process not multiple barge group");
        }
    }
}
