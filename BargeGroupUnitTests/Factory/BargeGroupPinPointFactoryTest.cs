﻿using BargeGroupingLogic.BargeGrouping;
using BargeGroupPinPointLogic.BargePinPointConfig;
using BargeGroupPinPointLogic.Factory;
using BargeGroupPinPointLogic.Factory.MultipleBargePinPoint;
using BargeGroupPinPointLogic.Factory.SingleBargeGroupPinPoint;
using BargeGroupPinPointLogic.Translators;
using Moq;
using NUnit.Framework;
using System.Collections.Generic;

namespace BargeGroupUnitTests.Factory
{
    [TestFixture]
    public class BargeGroupPinPointFactoryTest
    {
        IEnumerable<ISingleBargeFieldTranslator> bargeGroupFieldTranslators;
        IEnumerable<IMultipleBargeFieldTranslator> MultipleBargeGroupFieldTranslators;
        MultipleBargeGroupPinPointCreator multipleBgPpCreator;
        SingleBargeGroupPinPointCreator singleBgPpCreator;
        Barge barge1;
        Barge barge2;

        [SetUp]
        protected void SetUp()
        {
            bargeGroupFieldTranslators = new ISingleBargeFieldTranslator[] { new BargeIdFieldTranslator(), new RiverFieldTranslator() };
            MultipleBargeGroupFieldTranslators = new IMultipleBargeFieldTranslator[] { new CountFieldTranslator(), new RiverFieldTranslator() };
            multipleBgPpCreator = new MultipleBargeGroupPinPointCreator(MultipleBargeGroupFieldTranslators);
            singleBgPpCreator = new SingleBargeGroupPinPointCreator(bargeGroupFieldTranslators);
            barge1 = new Barge(1, "Barge 1", "Mississippi", 19.5);
            barge2 = new Barge(2, "Barge 11", "Minnesota", 19.5);
        }

        [Test]
        public void PinPoint_Creation_By_Factory()
        {
            BargeGroupPinPointConfiguration config = new BargeGroupPinPointConfiguration();
            config.SinglePinPointTitleFormat = "Single PinPoint";
            config.MultiplePinPointTitleFormat = "Multiple PinPoint";
            var mockConfigProvider = new Mock<IBargeGroupPinPointConfigService>();
            mockConfigProvider.Setup(foo => foo.GetConfig()).Returns(config);

            var bargeGr = new BargeGroup(barge1);

            BargeGroupPinPointFactory factory = new BargeGroupPinPointFactory(mockConfigProvider.Object, singleBgPpCreator, multipleBgPpCreator);
            var pinPoint = factory.GetFrom(bargeGr);

            Assert.AreEqual("Single PinPoint", pinPoint.Title);

            bargeGr.AddBarge(barge2);

            var pinPointMultiple = factory.GetFrom(bargeGr);

            Assert.AreEqual("Multiple PinPoint", pinPointMultiple.Title);

            //Test IEnumerable of SingleGroup, MultipleGroup, MixedGroup
        }
    }
}
