using BargeGroupingLogic.BargeGrouping;
using BargeGroupingLogic.BargeManager;
using BargeGroupPinPointLogic.Factory;
using System;

namespace BargeGroupingLogic
{
    /// <summary>
    /// BargeGroupMap "shows" groups of barges on a map.
    /// For the purpose of this test fill it in with code to print the barge groups to the Console.
    /// For example, in such format:
    /// 	Group #0, "2 Barges"
	/// 	  | Id            | River       | MileMarker | 
	///	      | Barge 1       | Mississippi | 19.5       | 
	///	      | Barge 2       | Mississippi | 19.6       | 
    /// 	Group #1, "Barge #3"
    /// 	  | Id            | River       | MileMarker | 
    ///	      | Barge #3      | Mississippi | 193.5      | 
    /// </summary>
    public class BargeGroupMap
    {
        private IBargeDataProvider bargeReader;
        IBargeGroupPinPointFactory pinPointFactory;

        public BargeGroupMap(IBargeDataProvider bargeReader, IBargeGroupPinPointFactory pinPointFactory)
        {
            this.bargeReader = bargeReader;
            this.pinPointFactory = pinPointFactory;
        }
        
        /// <summary>
        /// "Shows" the barge groups on a map.
        /// </summary>
        public async void ShowBargeGroupsAsync()
        {

            Manager.BargeManager bargeManager = new Manager.BargeManager();
            
            var bargeGroups = await bargeManager.GroupBargesAsync(bargeReader.GetBargeList());

            var pinPoints = pinPointFactory.GetFrom(bargeGroups);

            Console.WriteLine("Barge groups :");
            var count = 0;
            foreach (var group in pinPoints)
            {
                Console.WriteLine("Group: " + count++ + " " + group.ToString());
            }
            
        }

    }
}