﻿using BargeGroupingLogic.BargeGrouping;
using BargeGroupPinPointLogic.BargePinPointConfig;
using BargeGroupPinPointLogic.Factory;
using BargeGroupPinPointLogic.Factory.MultipleBargePinPoint;
using BargeGroupPinPointLogic.Factory.SingleBargeGroupPinPoint;
using BargeGroupPinPointLogic.Translators;
using System;
using System.Collections.Generic;
using System.Threading;

// See "Documentation" folder first.

namespace BargeGroupingLogic
{
    class Program
    {
        static Dictionary<char, Action> actionsDict = new Dictionary<char, Action>();
        static IBargeDataProvider bargeReader = new BargeDataProvider();
        static IBargeGroupPinPointConfigService configService = new BargeGroupPinPointConfigProvider();
        static IBargeGroupPinPointFactory pinPointFactory = GetPinPointFactory();
        static bool continueApp = true;

        static Program()
        {
            actionsDict.Add('1', SetMultipleBargeGroupTitleFormat);
            actionsDict.Add('2', SetSingleBargeGroupTitleFormat);
            actionsDict.Add('3', ShowPinPoints);
            actionsDict.Add('4', () => { continueApp = false; });
        }

        #region Initialization        

        static IBargeGroupPinPointFactory GetPinPointFactory()
        {
            return new BargeGroupPinPointFactory(GetConfigProvider(), GetISingleBargeGroupPinPointCreator(), GetIMultipleBargeGroupPinPointCreator());
        }

        static IBargeGroupPinPointConfigService GetConfigProvider()
        {
            return configService;
        }

        static ISingleBargeGroupPinPointCreator GetISingleBargeGroupPinPointCreator()
        {

            var fieldFormatters = new ISingleBargeFieldTranslator[]
            {
                new BargeIdFieldTranslator(),
                new CountFieldTranslator(),
                new MileMarkerFieldTranslator(),
                new RiverFieldTranslator()
            };

            return new SingleBargeGroupPinPointCreator(fieldFormatters);
        }

        static IMultipleBargeGroupPinPointCreator GetIMultipleBargeGroupPinPointCreator()
        {

            var fieldFormatters = new IMultipleBargeFieldTranslator[]
            {
                new CountFieldTranslator(),
                new MileMarkerFieldTranslator(),
                new RiverFieldTranslator()
            };

            return new MultipleBargeGroupPinPointCreator(fieldFormatters);
        }

        #endregion

        static void SetMultipleBargeGroupTitleFormat()
        {
            ClearConsole();

            var config = configService.GetConfig();

            Console.WriteLine("Multiple Barge Group title format:");
            Console.WriteLine("Current value is: " + config.MultiplePinPointTitleFormat);
            Console.WriteLine("Please enter new title format:");

            var titleFormat = Console.ReadLine();

            if (!string.IsNullOrEmpty(titleFormat))
            {
                config.MultiplePinPointTitleFormat = titleFormat;

                configService.SaveConfig(config);
            }

        }

        static void SetSingleBargeGroupTitleFormat()
        {
            ClearConsole();
            var config = configService.GetConfig();

            Console.WriteLine("Single Barge Group title format:");
            Console.WriteLine("Current value is: "+ config.SinglePinPointTitleFormat);
            Console.WriteLine("Please enter new title format:");

            var titleFormat = Console.ReadLine();

            if (!string.IsNullOrEmpty(titleFormat))
            {
                config.SinglePinPointTitleFormat = titleFormat;

                configService.SaveConfig(config);
            }
        }

        static void ShowPinPoints()
        {
            var bargeGroupMap = new BargeGroupMap(bargeReader, pinPointFactory);

            bargeGroupMap.ShowBargeGroupsAsync();

            Console.WriteLine("\nWaiting for results!!!\n");
            Console.WriteLine("Press any key after result is shown!\n");
            Console.ReadKey();
        }

        static void Main()
        {
            // The code below is provided as an example only. Feel free to modify it in any way you want.
            // However, keep the "brains" out of here - remember, this is just a start-up program.

            System.AppDomain.CurrentDomain.UnhandledException += CurrentDomain_UnhandledException;

            ShowMenu();
        }

        static void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            //Do some actions to log unexpected exception
            ClearConsole();

            Console.WriteLine("Oops, unexpected problems still happen! Would you like to continue? y/n");

            var userOption = Console.ReadKey();

            if (userOption.KeyChar == 'y')
            {
                ShowMenu();
            }
            else
            {
                ClearConsole();
                Console.WriteLine("Bye, bye dude!");
                Thread.Sleep(3000);
                Environment.Exit(1);
            }

        }

        static void ClearConsole()
        {
            Console.Clear();
        }

        static void ShowMenu()
        {
            do
            {
                ClearConsole();
                Console.WriteLine("Please choose your action:");
                Console.WriteLine("1. Set Multiple Barge Group title format");
                Console.WriteLine("2. Set Single Barge Group title format");
                Console.WriteLine("3. Show Barge group Pin Points");
                Console.WriteLine("4. Exit");
                var userOption = Console.ReadKey();

                if (actionsDict.ContainsKey(userOption.KeyChar))
                {
                    actionsDict[userOption.KeyChar]();
                }
            }
            while (continueApp);
        }
    }
}
