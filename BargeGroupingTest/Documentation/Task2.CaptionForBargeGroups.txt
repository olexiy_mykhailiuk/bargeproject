﻿Feature: Caption for barge groups
The PinPoint should have a title/label that provides some information about the group.
The Caption is configured for 2 cases:
	- a GroupNameFormat property for a group that contains multiple barges 
	- a SingleBargeNameFormat property for a group that contains a single barge 

Each property has a string value that acts like a format. Values like "Number of barges", 
"River name" and "Mile marker" can be injected into the caption string in some form.

See examples of the format string in the scenarios below.
PS. Those format strings are provided as examples. You may use your own formats if you prefer to.
	


Scenario: Default GroupNameFormat should be applied for groups with multiple barges
	Given The GroupNameFormat is "[Count] barges"
	Given Barges
		| Id            | River       | MileMarker | 
		| Barge 1       | Minnesota   | 125        | 
		| Barge 2       | Minnesota   | 125        | 
	When The barges are grouped
	Then The result should be 1 groups
	And Group #0 is named "2 barges"



Scenario: Default SingleBargeNameFormat should be applied for groups with a single barge
	Given The SingleBargeNameFormat is "1 barge, [Id]"
	Given Barges
		| Id            | River       | MileMarker | 
		| ABA45         | Mississippi | 125        | 
	When The barges are grouped
	Then The result should be 1 group
	And Group #0 is named "1 barge, ABA45"



Scenario: Custom fields can be injected into the caption for displaying single barges
	This scenario can be implemented in a number of ways (choose anyone you like): 
	1. Provide a public property on the class to set the caption string format
	2. Read the format string from application settings file
	3. Pass the format as a constructor parameter to the class
	4. Any other suitable way of externally injecting this behavior into your class implementation
	Given The SingleBargeNameFormat is "1 barge, [Id], river [River], mile [MileMarker]"
	Given Barges
		| Id            | River       | MileMarker | 
		| ABA45         | Mississippi | 125        | 
	When The barges are grouped
	Then The result should be 1 group
	And Group #0 is named "1 barge, ABA45, river Mississippi, mile 125"



Scenario: Custom fields can be injected into the caption for displaying barge groups
	This scenario can be implemented in a number of ways (choose anyone you like): 
	1. Provide a public property on the class to set the caption string format
	2. Read the format string from application settings file
	3. Pass the format as a constructor parameter to the class
	4. Any other suitable way of externally injecting this behavior into your class implementation
	Given The GroupNameFormat is "[Count] barges, river [River], mile [MileMarker]"
	Given Barges
		| Id            | River       | MileMarker | 
		| ABA45         | Mississippi | 125        | 
		| IN603         | Mississippi | 125        |
		| DRM1008       | Ohio        | 90         |
		| ACBL4743      | Ohio        | 90         |
	When The barges are grouped
	Then The result should be 2 groups
	And Group #0 is named "2 barges, river Mississippi, mile 125"
	And Group #1 is named "2 barges, river Ohio, mile 90"